<?php 
require_once ("inc/config.php");
require_once ("inc/functions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="en" class="no-js">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">
		<title>Ultra Premium direct</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		
		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://en.naturaplusultra.com"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content=""/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>

		<link rel="stylesheet" href="<?php echo _INSTDIR_; ?>css/owl.carousel.css" type="text/css" media="screen" />

		<?php include_once("header.php"); ?>

		<!-- 1er block - Slider -->
		<div class="cols-row end begin">
			<div class="col-100">
				<div class="owl-carousel">
					<div class="item">
						<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food">
						<img class="width-100 block lazy" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" 
						    data-original="<?php echo _INSTDIR_; ?>img/slides/slide-1.jpg" 
						    data-w320="<?php echo _INSTDIR_; ?>img/slides/slide-1_320.jpg" 
						    data-w480="<?php echo _INSTDIR_; ?>img/slides/slide-1_480.jpg"
						alt="..."/>
						</a>
					</div>
					<div class="item">
						<a href="<?php echo _INSTDIR_; ?>grain-free-dry-dog-food">
						<img class="width-100 block lazy" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" 
						    data-original="<?php echo _INSTDIR_; ?>img/slides/slide-2.jpg" 
						    data-w320="<?php echo _INSTDIR_; ?>img/slides/slide-2_320.jpg" 
						    data-w480="<?php echo _INSTDIR_; ?>img/slides/slide-2_480.jpg"
						alt="..."/>
						</a>
					</div>
					<div class="item">
						<a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food">
							<img class="width-100 block lazy" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" 
						    data-original="<?php echo _INSTDIR_; ?>img/slides/slide-3.jpg" 
						    data-w320="<?php echo _INSTDIR_; ?>img/slides/slide-3_320.jpg" 
						    data-w480="<?php echo _INSTDIR_; ?>img/slides/slide-3_480.jpg"
						alt="..."/>
						</a>
					</div>
				</div>
			</div>
		</div>

		<!-- 2eme block - Products -->
		<div class="container">
			<div class="products">
				<div class="cols-row">
					<div class="col-33 centered">
						<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food">
							<div class="products__item">
								<img src="<?php echo _INSTDIR_; ?>img/low_grain/gamme_low_grain.png" alt="">
							</div>
						</a>
						<h4 class="products__name">Low grain dog food</h4>
						<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food" class="btn upper width-100">Learn more ></a>
					</div>
	
					<div class="col-33 centered">
						<a href="<?php echo _INSTDIR_; ?>grain-free-dry-dog-food">
							<div class="products__item">
								<img src="<?php echo _INSTDIR_; ?>img/grain_free/gamme_grain_free.png" alt=""> 
							</div>
						</a>
						<h4 class="products__name">Grain free dog food</h4>
						<a href="<?php echo _INSTDIR_; ?>grain-free-dry-dog-food" class="btn upper width-100">Learn more ></a>
					</div>
	
					<div class="col-33 centered">
						<a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food">
							<div class="products__item">
								<img src="<?php echo _INSTDIR_; ?>img/cat/gamme_dry_wet_chat.png" alt="">
							</div>
						</a>
						<h4 class="products__name">Grain free cat food</h4>
						<a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food" class="btn upper width-100">Learn more ></a>
					</div>
				</div>
			</div>
		</div>

		<!-- 3eme block - Image produits -->
		<div class="products-skills-bleu no-mob">
			<div class="container begin">
				<img src="<?php echo _INSTDIR_; ?>img/homepage/gamme_homepage.jpg" class="width-100 block"alt="">
			</div>

		</div>

		<!-- 4eme block - Avis verifies -->
		<div class="product-gamme cols-row end begin ekomi">

			<div class="container begin end">
				<img class="block lazy ekomi-img" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" 
				    data-original="<?php echo _INSTDIR_; ?>img/ekomi.jpg" 
				    data-w320="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
				    data-w480="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
				alt="..."/>
			</div>

		</div>

		<!-- 5eme block - Descriptions -->
		<div class="container-argument">
			<div class="cols-row">
				<div class="col-flex">
					<div class="col-50 home-argument">
						<a href="<?php echo _INSTDIR_; ?>our-philosophy">
							<div class="panel">
								<div class="panel_img">
									<img src="<?php echo _INSTDIR_; ?>img/homepage/our_philosophy_homepage.jpg" alt="">
								</div>
								<div class="panel_text">
									<h3>100% high QUALITY INGREDIENTS</h3>
									<p>
									People from ULTRA PREMIUM DIRECT are convinced that diet has a huge impact on pets health and well-being. That is why we propose the best quality for complete and balanced premium food. Our companion dogs and cats are carnivore. All our formulas are based on pet’s ancestral diet. That is why our recipes are rich in meat and protein of animal origin just as nature intended. Our recipes are veterinary formulated. Our first ingredient is and will remain meat. Our formulas have a low glycemic index to prevent risks from diabetes and obesity: low grain (mainly rice) or grain free.
								</p>
								</div>
							</div>
						</a>
					</div>
					<div class="col-50 home-argument">
						<a href="<?php echo _INSTDIR_; ?>our-philosophy">
							<div class="panel">
								<div class="panel_img">
									<img src="<?php echo _INSTDIR_; ?>img/homepage/our_philosophy_homepage_2.jpg" alt="">
								</div>
								<div class="panel_text">
								<h3>strong pet food expertise</h3>
									<p>
										Factory of 10 000 m2 and head office located in the South West of France.<br>
										- Approved site by veterinary department.<br>
										Dog and Cat premium food manufacturing and marketing.<br>
										- Veterinary formulated.<br>
									</p>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>

		<!-- 6eme block - Image -->
		<div class="cols-row begin end no-mob">
				<img src="<?php echo _INSTDIR_; ?>img/banner-only-best.jpg" class="width-100 block"alt="">		
		</div>	
				
		<?php include_once("footer.php"); ?>
		
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/owl.carousel.min.js"></script>

		<script>

			$('.owl-carousel').owlCarousel({
			    loop:true,
			    margin:0,
			    dots: false,
			    autoplay: true,
			    autoplayTimeout: 5000,
			    autoplaySpeed: 2000,
			    navSpeed: 1500,
			    responsiveClass:true,
			    responsive:{
			        0:{
			            items:1,
			            nav:false
			        },
			        600:{
			            items:1,
			            nav:false
			        },
			        1000:{
			            items:1,
			            nav:true,
			            navText: [
					      "<img src='<?php echo _INSTDIR_; ?>img/left.png'>",
					      "<img src='<?php echo _INSTDIR_; ?>img/right.png'>"
					      ],
			            loop:true
			        }
			    }
			})
		</script>

		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>
	</body>
</html>