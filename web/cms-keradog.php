<?php 
require_once ("inc/config.php");
require_once ("inc/functions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="en" class="no-js">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">
		<title>Ultra Premium direct</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		
		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://en.naturaplusultra.com"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content=""/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>

		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/modal.js"></script>
		<link rel="stylesheet" href="<?php echo _INSTDIR_; ?>css/modal.css">

		<?php include_once("header.php"); ?>

        <!-- 1er block -->
		<div class="cols-row begin end">
			<img src="<?php echo _INSTDIR_; ?>img/keradog/tagada_made_in_fr-min.jpg" class="width-100" alt="">
		</div>

		<!-- 2eme block -->
		<div class="container">
			<div class="cols-row">

				<div class="col-33 centered home-product">
					<a href="#"><img src="<?php echo _INSTDIR_; ?>img/keradog/home-product-1.png" class="keradog-popup-1" alt=""></a>
					<h3 class="title-heading">
						<span class="title-heading--main">Gentle shampoo</span>
						<span class="title-heading--sub">Sulfate free</span>
					</h3>
					<a class="btn width-100 keradog-popup-1" href="#">More ></a>
				</div>

				<div class="col-33 centered home-product">
					<a href="#"><img src="<?php echo _INSTDIR_; ?>img/keradog/home-product-2.png" class="keradog-popup-2" alt=""></a>
					<h3 class="title-heading">
						<span class="title-heading--main">2 in 1 shampoo</span>
						<span class="title-heading--sub">Conditionner for long and mid long hair</span>
					</h3>
					<a class="btn width-100 keradog-popup-2" href="#">More ></a>

				</div>

				<div class="col-33 centered home-product">
					<a href="#"><img src="<?php echo _INSTDIR_; ?>img/keradog/home-product-3.png" class="keradog-popup-3" alt=""></a>
					<h3 class="title-heading">
						<span class="title-heading--main">No rinse shampoo</span>
						<span class="title-heading--sub">24h fresh effect</span>
					</h3>
					<a class="btn width-100 keradog-popup-3" href="#">More ></a>

				</div>
			</div>
		
		</div>

		<!-- Modal Product 1 -->
		<div class="cd-popup-1">
			<div class="cd-popup-container">
				<div class="container modal-container">
					<!-- 1er block -->
					<div class="cols-row">
						<div class="col-20">
							<div class="keradog-modal-img">
								<img src="<?php echo _INSTDIR_; ?>img/keradog/gentle_shampoo_x2-min.png" class="keradog-product-img" alt="">
							</div>
						</div>
						<div class="col-80">
							<div class="cols-row">
								<div class="col-100">
									<span class="keradog-label"></span>
									<span class="keradog-label-text">For dogs and puppies</span>
								</div>
							</div>
							<div class="cols-row">
								<div class="col-100">
									<span class="heading-keradog-modal-primary">Gentle shampoo</span>
									<span class="heading-keradog-modal-sub">Sulfate free</span>
								</div>
							</div>
							<div class="cols-row">
								<div class="col-100">
									<p>Keradog’s gentle shampoo is formulated paraben-free and sulfate-free, and is adapted for dogs that has a sensitive skin. The Keratin softness formula deeply cleans the coat of your dog or puppy to avoid any irritation.</p>
									<p>Keradog’s gentle shampoo intensely hydrates the coat and soothes the sensitive skin of your animal. It’s perfect for dogs that tend to have itches, blotches or allergies. The subtle combination of Keratin and pH adapted to your dog’s skin respects the animal’s epidermis and takes care of his coat.</p>
									<p>The specific attributes of keratin allow to reinforce the hair’s firmness and guarantee a complete nutrition for the beauty of your dog’s coat. The expertise and the French savoir-faire in cosmetics are reunited in this shampoo, revealing the beauty and the well-being of your animals.</p>
								</div>
							</div>
						</div>
					</div>

					<!-- 2eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Benefits of the gentle shampoo keradog: </h3>
							<div class="keradog-modal-list">
								1. Formula sulfate-free and paraben-free perfect for tender skins<br>
								2. Eliminate impurities and purifies the coat very finely<br>
								3. Thanks to its nutritional attributes it appeases, nourishes deeply and hydrate the skin of your animal<br>
								4. The coat gets back to a brightness and a softness unequalled<br>
								5. Keratin allows to prevent the loss of hair and stimulate their growth<br>
							</div>
						</div>
					</div>

					<!-- 3eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Directions of use: </h3>
							<p>To begin with, shake the gentle shampoo before any use. Spread it on the coat and rub softly. Then you can rinse. Do not hesitate to do it again, then let the shampoo sit for about 2 minutes.  Rinse plentifully the coat of your animal and wipe with a towel. </p>
						</div>
					</div>

					<!-- 4eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Results: </h3>
							<p>The coat is glittering with brightness, full of vitality and very soft. The coat is intensely revitalized and more resistant. A unique result, lasting and dazzling with beauty.</p>
						</div>
					</div>

					<!-- 5eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Cautions for use</h3>
							<p>Do not swallow, avoid contact with the eyes. If there’s any, rinse thoroughly. Wash your hands after application.</p>
						</div>
					</div>
				</div>
				<a href="#0" class="cd-popup-close img-replace">Close</a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->

		<!-- Modal Product 2 -->
		<div class="cd-popup-2">
			<div class="cd-popup-container">
				<div class="container modal-container">
					<!-- 1er block -->
					<div class="cols-row">
						<div class="col-20">
							<div class="keradog-modal-img">
								<img src="<?php echo _INSTDIR_; ?>img/keradog/2_in_1_shampoo_x2-min.png" class="keradog-product-img" alt="">
							</div>
						</div>
						<div class="col-80">
							<div class="cols-row">
								<div class="col-100">
									<span class="keradog-label"></span>
									<span class="keradog-label-text">for dogs, puppies and cats</span>
								</div>
							</div>
							<div class="cols-row">
								<div class="col-100">
									<span class="heading-keradog-modal-primary">2 in 1 Shampoo</span>
									<span class="heading-keradog-modal-sub">Conditioner for long and mid-long hair</span>
								</div>
							</div>
							<div class="cols-row">
								<div class="col-100">
									<p>The coat of your animal needs to be looked after to reveal all of its beauty and its radiance. This is why we have created Keradog’s conditioner-shampoo 2 in 1. This Keratin enriched shampoo is a complete care for the coat of your animal. It is specifically made for cats and dogs that has long and mid-long hair.</p>
									<p>Our conditioner shampoo 2 in 1 Keradog is enriched with keratin, a natural protein that soak into the heart of your animal’s coat. This 2 in 1 shampoo combines a keratin formula and a pH adapted to cats’ and dogs’ coats in order to reinforce, tone up and bring brightness and softness. The coat of your cat or dog is now sublimated.</p>
								</div>
							</div>
						</div>
					</div>

					<!-- 2eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Benefits of the Conditioner 2 in 1 Keradog :</h3>
							<div class="keradog-modal-list">
							1. The 2 in 1 action of the shampoo cleans and smooths your dog’s hair<br>
							2. The coat is deeply nourished<br>
							3. Keratin reveals an ultimate brightness and a silky coat<br>
							4. Limit the loss of hair and stimulate its growth<br>
							5. The coat is soft and supple<br>
							</div>
						</div>
					</div>

					<!-- 3eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Conditions for use :</h3>
							<p>To begin with, shake the gentle shampoo before any use. Spread it on the coat and rub softly. Then you can rinse. Do not hesitate to do it again, then let the shampoo sit for about 2 minutes.  Rinse plentifully the coat of your animal and wipe with a towel. </p>
						</div>
					</div>

					<!-- 4eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Results: </h3>
							<p>The hair is strong inside and bright outside. The coat is now smooth for a soft brushing, a real moment of happiness for your animal.</p>
						</div>
					</div>

					<!-- 5eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Cautions for use</h3>
							<p>Do not swallow, avoid contact with the eyes. If there’s any, rinse thoroughly. Wash your hands after application.</p>
						</div>
					</div>
				</div>
				<a href="#0" class="cd-popup-close img-replace">Close</a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->

		<!-- Modal Product 3 -->
		<div class="cd-popup-3">
			<div class="cd-popup-container">
				<div class="container modal-container">
					<!-- 1er block -->
					<div class="cols-row">
						<div class="col-20">
							<div class="keradog-modal-img">
								<img src="<?php echo _INSTDIR_; ?>img/keradog/no_rinse_shampoo_x2-min.png" class="keradog-product-img" alt="">
							</div>
						</div>
						<div class="col-80">
							<div class="cols-row">
								<div class="col-100">
									<span class="keradog-label"></span>
									<span class="keradog-label-text">for dogs, puppies and cats</span>
								</div>
							</div>
							<div class="cols-row">
								<div class="col-100">
									<span class="heading-keradog-modal-primary">No rinse shampoo</span>
									<span class="heading-keradog-modal-sub">24h Fresh Effect</span>
								</div>
							</div>
							<div class="cols-row">
								<div class="col-100">
									<p>Keradog’s dry shampoo is a rinse-less froth that allows a quick and effective cleaning. Enriched with Keratin, it absorbs sebum’s excess hold in your cat or dog’s coat, and makes it glaring with brilliance and softness.</p>
									<p>Keradog’s dry shampoo is enriched with Keratin, a natural protein gold in the hair, which respects the coat of your animal. This shampoo doesn’t aggress or irritate the skin, allowing a regular use on your cat or dog. Keratin repairs and nourishes the hair, it became soft and finds again a softness and an incomparable suppleness. Both gourmet and light, its perfume brings a true feeling of freshness to the coat thanks to its fine scent of Vervain/Lemon.</p>
								</div>
							</div>
						</div>
					</div>

					<!-- 2eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Benefits of Keradog’s dry shampoo :</h3>
							<div class="keradog-modal-list">
								1. Allows a simple and quick cleaning<br>
								2. Freshness effect 24h thanks to an enchanting perfume<br>
								3. A clean and fresh coat immediately <br>
								4. A froth easy to apply, rinse-less<br>
								5. The coat regains its softness and an ultimate smoothness<br>
							</div>
						</div>
					</div>

					<!-- 3eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Directions for use :</h3>
							<p>Shake the bottle of shampoo before using it. Then apply the shampoo on the coat of your animal, rub softly. Do not hesitate to do it again, then let the shampoo sit for about 2 minutes and dry the excess of product softly off your animal with a towel.</p>
						</div>
					</div>

					<!-- 4eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Results: </h3>
							<p>In the blink of an eye, the coat of your animal is exceptionally clean, silky and fresh. </p>
						</div>
					</div>

					<!-- 5eme block -->
					<div class="cols-row">
						<div class="col-100">
							<h3 class="keradog-h3">Use precautions :</h3>
							<p>Do not swallow, avoid contact with the eyes. If there’s any, rinse thoroughly. Wash your hands after application.</p>
						</div>
					</div>
				</div>
				<a href="#0" class="cd-popup-close img-replace">Close</a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->

        <!-- 3eme block -->
		<div class="container">
			<div class="description">
				<img src="<?php echo _INSTDIR_; ?>img/keradog/graphic_keratin-min.png" alt="" style="width: 20%">
				<span>
					Keratin is a natural protein constituting of the raw material of cats’ and dogs’ hair (as well as human hair). Thanks to Keratin, the coat is brighter, reinforced and has more weight.
				</span>
			</div>
		</div>

		<!-- 4eme block -->
		<div class="description_1">
			<div class="grey-block">
				<div class="container">
					<div class="grey-block__text-box">
						<h3>But what about animals ?</h3>
						<p>
						Keratin is made with keratinocyte, the cells that constitute 90% of the skin. It insures the fur’s waterproofness and protects it against external agents. Keratin plays a very important role in cosmetics for cats and dogs: it’s the first component of the hair!					</p>
					</div>
					<div class="grey-block__text-box">
						<h3>Keradog's range</h3>
						<p>
						Made in France, our shampoos are formulated to clean the coat without attacking your dogs and cats’ fur or causing itches. Their pH is very specific. Our range of shampoos allows to slow down the loss of hair as well as it reinforce the coat of the animal.					</p>
					</div>
				</div>
			</div>
		</div>

		<!-- 5eme block -->
		<div class="container">
			<div class="products">
				<div class="cols-row">
					<div class="col-33 centered">
						<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food">
							<div class="products__item">
								<img src="<?php echo _INSTDIR_; ?>img/low_grain/gamme_low_grain.png" alt="">
							</div>
						</a>
						<h4 class="products__name">Low grain dog food</h4>
						<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food" class="btn upper width-100">Learn more ></a>
					</div>
	
					<div class="col-33 centered">
						<a href="<?php echo _INSTDIR_; ?>grain-free-dry-dog-food">
							<div class="products__item">
								<img src="<?php echo _INSTDIR_; ?>img/grain_free/gamme_grain_free.png" alt="">
							</div>
						</a>
						<h4 class="products__name">Grain free dog food</h4>
						<a href="<?php echo _INSTDIR_; ?>grain-free-dry-dog-food" class="btn upper width-100">Learn more ></a>
					</div>
	
					<div class="col-33 centered">
						<a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food">
							<div class="products__item">
								<img src="<?php echo _INSTDIR_; ?>img/cat/gamme_dry_wet_chat.png" alt="">
							</div>
						</a>
						<h4 class="products__name">Grain free cat food</h4>
						<a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food" class="btn upper width-100">Learn more ></a>
					</div>
				</div>
			</div>
		</div>

        <!-- 6eme block -->
		<div class="product-gamme cols-row end begin ekomi" style="border-top: 1px solid #ccc;">
			<div class="container begin end">
				<img class="ekomi-img block lazy" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" 
					data-original="<?php echo _INSTDIR_; ?>img/ekomi.jpg" 
					data-w320="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
					data-w480="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
				alt="..."/>
			</div>
		</div>

        <!-- 7eme block -->
        <div class="cols-row begin end no-mob">
			<img src="<?php echo _INSTDIR_; ?>img/banner-only-best.jpg" class="width-100 block"alt="">		
        </div>
        
		<?php include_once("footer.php"); ?>
		
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>