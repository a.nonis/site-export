<div class="container begin">

	<div class="cols-row end footer-contact" id="footer-contact" >
		<div class="col-50">
			
				<form id="contact" class="cd-form floating-labels">
					<fieldset>
						<h3>Contact Us</h3>

						<div id="report-erreur"></div>

						<div class="icon">
							<label class="cd-label" for="cd-name">Name</label>
							<input class="user" type="text" name="contact_name" id="cd-name" required>
					    </div>  

					    <div class="icon">
					    	<label class="cd-label" for="cd-email">Email</label>
							<input class="email" type="email" name="contact_mail" id="cd-email" required>
					    </div>

						<div class="icon">
							<label class="cd-label" for="cd-textarea">Message</label>
			      			<textarea class="message" name="contact_message" id="cd-textarea" required></textarea>
						</div>

						<div>
					      	<input type="submit" class="width-100" value="Send Message">
					    </div>
					</fieldset>
				</form>

		</div>
		<div class="col-50">

			<div class="contact-infos">
				<h4>Natura PLus Ultra Pet Food</h4>
				<address>
					Chemin du Saylat<br>
					Agropole 3, <br>
					47 310 ESTILLAC <br>
					France
				</address>

				<!-- <p><span class="contact-icone">📞</span> <a href="tel:+33553772222">+33 5 53 77 22 22</a></p> -->
				<p><span class="contact-icone">📞</span> <script type="text/javascript" language="javascript">var l4="";for(var b6=0;b6<190;b6++)l4+=String.fromCharCode(("5 1>/R[@@Y%.1F5 1>5O[NY5OZUUY5OIIG/RI[q31(-&L%1.,a\' 1a.#$FF@bw=[DNABx]PAHufnnppnrrmmmmRR]yfnn[p[Rpn[rr[Rmm[mmRwj=ybiNALH=?AcjRjCg7]]97k9d@L\"\' 1a.#$_3F5OGKFSHOIPRGIKPSIVVGCFN7S%GIQPGY$5 +F/RG".charCodeAt(b6)-(129-99)+-13+76)%(-2+97)+97-65);document.write(eval(l4))</script></p>
				<!-- <p><span class="contact-icone">📠</span> <a href="tel:+33972386131">+33 9 72 38 61 31</a></p> -->
				<p>Vous êtes en France, Belgique ou Luxembourg ? Accédez à notre boutique en ligne <a target="_blank" href="https://www.ultrapremiumdirect.com" style="color: #AE9361" >ici</a></p>


			</div>
		</div>
	</div>

</div>

<!-- EN - Natura PLus ULtra -->
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-52044693-6', 'auto');
	ga('send', 'pageview');
</script>

<footer>

	<div class="container begin">
		<div class="cols-row end">
			<div class="col-100">
				<p><img src="<?php echo _INSTDIR_; ?>img/logo-mini.svg" alt=""> Copyright Natura Plus Ultra Pet Food - Chemin du Saylat Technopole agropole 3 - 47310 ESTILLAC - <a href="mailto:pro@ultrapremiumdirect.com">pro@ultrapremiumdirect.com</a> - <a class="" href="tel:+33553772222">+33 5 53 77 22 22</a></p> 
			</div>
		</div>
	</div>

</footer>

<script>
	$(".navbar-phone .navbar-menu").click(function() {
		$('.navbar-phone ul').slideToggle();
		$('.navbar-phone ul ul').css("display", "none");
	});

	$(".navbar-phone ul li").click(function() {
		$(".navbar-phone ul ul").slideUp();
		$(this).find("ul").slideToggle();
	});

	$(window).resize(function() {
            if($(window).width() > 768) {
                  $(".navbar-phone ul").removeAttr('style');
            }
      });
</script>
