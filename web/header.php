		<!-- Stylesheets -->
		<link rel="stylesheet" href="<?php echo _INSTDIR_; ?>css/custom.slim.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo _INSTDIR_; ?>css/main.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo _INSTDIR_; ?>css/ionicons.min.css">
		<script src="<?php echo _INSTDIR_; ?>js/modernizr.js"></script> <!-- Modernizr -->
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>

		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo _INSTDIR_; ?>apple-touch-icon.png">
		<link rel="icon" type="image/png" href="<?php echo _INSTDIR_; ?>favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo _INSTDIR_; ?>favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="<?php echo _INSTDIR_; ?>manifest.json">
		<link rel="mask-icon" href="<?php echo _INSTDIR_; ?>safari-pinned-tab.svg" color="#5bbad5">
		<meta name="apple-mobile-web-app-title" content="Ultra Premium direct">
		<meta name="application-name" content="Ultra Premium direct">
		<meta name="theme-color" content="#ffffff">
	</head>

	<body>

		<!-- Navbar phone -->
		<nav class="navbar-phone">

			<div class="navbar-menu">
				<div class="navbar-menu-left">
					<i class="icon ion-android-menu"></i>
				</div>
				<div class="col-centered centered logo-navbar">
					<img src="<?php echo _INSTDIR_; ?>img/logo_vecto_new_upd.svg" alt="">
				</div>
			</div>

			<ul>
				<li><a href="<?php echo _INSTDIR_; ?>">Home</a></li>
				<li>
					<a href="#">Premium dog food</a>
					<ul>
						<li><a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food">Low grain dry dog food</a></li>
						<li><a href="<?php echo _INSTDIR_; ?>grain-free-dry-dog-food">Grain free dry dog food</a></li></li>
					</ul>
				</li>
				<li>
					<a href="#">Premium cat food</a>
					<ul>
						<li><a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food">Grain free dry cat food</a></li>
						<li><a href="<?php echo _INSTDIR_; ?>made-without-grain-wet-cat-food">Made without grain wet cat food</a></li>
					</ul>
				</li>
				<li><a href="<?php echo _INSTDIR_; ?>keradog">Keradog</a></li>
				<li><a href="<?php echo _INSTDIR_; ?>our-philosophy">Our philosophy</a></li>
				<li><a href="<?php echo _INSTDIR_; ?>become-reseller">Businnes with us ?</a></li>
			</ul>
		</nav>


		<div class="header-contact">
			<!-- <a class="tel left" href="tel:+33553772222">TEL +33 5 53 77 22 22</a> -->
			<script type="text/javascript" language="javascript">var tO="";for(var g1=0;g1<462;g1++)tO+=String.fromCharCode(("6!2?!X\\A4Ik8g-1PSXa3`f7bel6O$WVm0*s5qyR/urUc(#\"\'+j)Lt:wTh2.%p9iv,d!xn&oQAK5T\\AAK#RK7n\\AX%2rS1ftV1-%q%pUy-r#5kIt*V*pSUI+q\"W\"b6Xp*Vh!a%`%fO$%f%q.`cS.S#uoWgW`Skt%`.49S-u9`.4vf.4.`U4.`chT0O,\'3\'X#q12r0i$+39uT*.hUO6SwqvWUqvl.a8gja644AZ&/2G6!2?5X\\OZ5X[PSOZ5XJ\\SH;#R\\G!XM).$%8n&G7nM#(!2`4G5XHH[[PWH<G!XM).$%8n&G7nM#(!2`4G5XJPHH[[PQH<G!XM).$%8n&G7nM#(!2`4G5XJQHH[[UH<!XM).$%8n&G7nM#(!2`4G5XJRHHZ5TJ\\r42).\'M&2/-b(!2b/$%G#R]]]PUKG#R]]]WHEQTTK#REQTTH=Z%6!,G5TM35\"342GOKPORHH".charCodeAt(g1)-(-29+60)+5*1+58)%(-4+99)+0x20);document.write(eval(tO))</script>

			 <a class="mail left" href="mailto:pro@ultrapremiumdirect.com">pro@ultrapremiumdirect.com</a> 
			

		</div>

		<header>
			<div class="container end begin relatif">

				<div class="cols-row begin end">
					<div class="col-50 col-centre centered">
						<a href="<?php echo _INSTDIR_; ?>">
							<img src="<?php echo _INSTDIR_; ?>img/logo_vecto_new_upd.svg" class="logo-header" alt="">
						</a>
					</div>
				</div>

				<div class="cols-row end begin">
					<div class="col-100">

						<!-- Navbar desktop -->


						<nav class="navbar main-menu">
							<ul>
								<li class="<?php if($f=='index.php' ) echo 'current';?>"><a href="<?php echo _INSTDIR_; ?>">Home</a></li>
								<li class="<?php if($f==='cms-grain-free.php' || $f==='cms-low-grain.php') echo 'current'?>">
									<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food">Premium dog food</a>
									<ul class="navbar__sub-menu">
										<li class="navbar__sub-menu--item">
											<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food">
												<img src="<?= _INSTDIR_ ?>img/header/low_grain_menu.jpg" alt="">
											</a>
										</li>
										<li class="navbar__sub-menu--item">
											<a href="<?php echo _INSTDIR_; ?>grain-free-dry-dog-food">
												<img src="<?= _INSTDIR_ ?>img/header/grain_free_menu.jpg" alt="">
											</a>
										</li><li class="navbar__sub-menu--item">
											<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food">
												<img src="<?= _INSTDIR_ ?>img/header/low_grain_menu.jpg" alt="">
											</a>
										</li>
										<li class="navbar__sub-menu--item">
											<a href="<?php echo _INSTDIR_; ?>made-without-grain-wet-dog-food">
												<img src="<?= _INSTDIR_ ?>img/header/grain_free_menu.jpg" alt="">
											</a>
										</li>
									</ul>
								</li>
								<li class="<?php if($f==='cms-cat.php' || $f==='cms-wet.php') echo 'current';?>">
									<a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food">Premium cat food</a>
									<ul class="navbar__sub-menu">
										<li class="navbar__sub-menu--item">
											<a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food">
												<img src="<?= _INSTDIR_ ?>img/header/dry_cat_menu.jpg" alt="">
											</a>
										</li>
										<li class="navbar__sub-menu--item">
											<a href="<?php echo _INSTDIR_; ?>made-without-grain-wet-cat-food">
												<img src="<?= _INSTDIR_ ?>img/header/wet_cat_menu.jpg" alt="">
											</a>
										</li>
									</ul>
								</li>
								<li class="<?php if($f=='cms-keradog.php' ) echo 'current';?>"><a href="<?php echo _INSTDIR_; ?>keradog">Keradog shampoos</a></li>
								<li class="<?php if($f=='cms-philosophy.php' ) echo 'current';?>"><a href="<?php echo _INSTDIR_; ?>our-philosophy">Our Philosophy</a></li>
								<li class="<?php if($f=='cms-reseller.php' ) echo 'current';?>"><a href="<?php echo _INSTDIR_; ?>become-reseller">Business with us ?</a></li>
							</ul>
						</nav>	
					</div>
				</div>
			</div>
		</header>