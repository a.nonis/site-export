<?php 
require_once ("inc/config.php");
require_once ("inc/functions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="en" class="no-js">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">
		<title>Ultra Premium direct</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		
		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://en.naturaplusultra.com"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content=""/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>

		<?php include_once("header.php"); ?>

		<div class="width-100 grey-back padding-50">
			<div class="container reseller begin">
				
				<!-- 1er block -->
				<div class="cols-row">
					<div class="col-100">
						<h1>Become a Reseller / Distributor / Importator</h1>
						<p>Please complete the following inquiry form. We will be in touch with information and policies regarding our wholesale pet food program.</p>
					</div>
				</div>
	
				<!-- 2eme block -->
				<div class="cols-row">
					<div class="col-90 form col-centre">
						<form id="reseller" class="form-reseller">
							<h2>Inquiry Form</h2>
							<p>Please fill out all information</p>
	
							<div class="cols-row begin">
								
								<div class="col-50">
									<input id="firstname" class="input width-100 facultatif" placeholder="First Name" type="text" name="firstname">
								</div>
	
								<div class="col-50">
									<input id="lastname" class="input width-100 facultatif" placeholder="Last Name" type="text" name="lastname">
								</div>
							</div>
	
							<div class="cols-row">
								
								<div class="col-50">
									<input id="company" class="input width-100 facultatif" placeholder="Company Name" type="text" name="company">
								</div>
	
								<div class="col-50">
									<input id="tax" class="input width-100 facultatif" placeholder="Tax Id#" type="text" name="tax">
								</div>
							</div>
	
							<div class="cols-row">
								
								<div class="col-50">
									<input id="phone" class="input width-100 facultatif" placeholder="Phone Number" type="text" name="phone">
								</div>
	
								<div class="col-50">
									<input id="email" class="input width-100 facultatif" placeholder="Email" type="email" name="email">
								</div>
							</div>
	
							<h3>Address</h3>
	
							<div class="cols-row begin">
								
								<div class="col-100">
									<input id="address" class="input width-100 facultatif" placeholder="Address" type="text" name="address">
								</div>
	
							</div>
	
							<div class="cols-row">
								
								<div class="col-50">
									<input id="city" class="input width-100 facultatif" placeholder="City" type="text" name="city">
								</div>
	
								<div class="col-50">
									<input id="state" class="input width-100 facultatif" placeholder="State" type="text" name="state">
								</div>
							</div>
	
							<div class="cols-row">
								
								<div class="col-50">
									<input id="postalcode" class="input width-100 facultatif" placeholder="Postal /Zip Code" type="text" name="postalcode">
								</div>
	
								<div class="col-50">
									<input id="contry" class="input width-100 facultatif" placeholder="Country" type="text" name="contry">
								</div>
							</div>
	
							<h3>About your business</h3>
							
							<div class="cols-row">
								<div class="col-100">
									<label><input id="checkcustomer" type="checkbox" value="Sales to End Customer" name="customer"> Sales to End Customer</label>
									<br>
									<label><input id="checkwholesale" type="checkbox" value="Wholesale, Distributor" name="wholesale"> Wholesale, Distributor</label>	
									<br>
									<label><input id="checkinternet" type="checkbox" value="Internet" name="internet"> Internet</label>	
									<br>
									<label><input id="checkshop" type="checkbox" value="Shop" name="shop"> Shop</label>	
								</div>
							</div>
	
							<div class="cols-row">
								<div class="col-100">
									<textarea name="commentaire" placeholder="Comments / Questions" id="" cols="30" rows="10"></textarea>
								</div>
							</div>
	
							<div class="cols-row begin end">
								<div class="col-100 centered">
									<img src="<?php echo _INSTDIR_; ?>img/load.gif" id="loader" alt="">
									<div id="report-erreur"></div>
								</div>
							</div>
							
							<div class="cols-row end">
								<div class="col-100">
									<input type="submit" class="width-100" value="submit">
								</div>
							</div>
	
	
						</form>
					</div>
				</div>
			</div>	
		</div>

		<!-- 3eme block - Products -->
		<div class="container">
			<div class="products">
				<div class="cols-row">
					<div class="col-90 col-centre">
						<div class="col-33 centered">
							<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food">
								<div class="products__item">
									<img src="<?php echo _INSTDIR_; ?>img/low_grain/gamme_low_grain.png" alt="">
								</div>
							</a>
							<h4 class="products__name">Low grain dog food</h4>
							<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food" class="btn upper width-100">Learn more ></a>
						</div>
		
						<div class="col-33 centered">
							<a href="<?php echo _INSTDIR_; ?>grain-free-dry-dog-food">
								<div class="products__item">
									<img src="<?php echo _INSTDIR_; ?>img/grain_free/gamme_grain_free.png" alt="">
								</div>
							</a>
							<h4 class="products__name">Grain free dog food</h4>
							<a href="<?php echo _INSTDIR_; ?>grain-free-dry-dog-food" class="btn upper width-100">Learn more ></a>
						</div>
		
						<div class="col-33 centered">
							<a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food">
								<div class="products__item">
									<img src="<?php echo _INSTDIR_; ?>img/cat/gamme_dry_wet_chat.png" alt="">
								</div>
							</a>
							<h4 class="products__name">Grain free cat food</h4>
							<a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food" class="btn upper width-100">Learn more ></a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- 4eme block - Avis verifies -->
		<div class="product-gamme cols-row end begin ekomi">
			<div class="container begin end">
				<img class="ekomi-img block lazy" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" 
					data-original="<?php echo _INSTDIR_; ?>img/ekomi.jpg" 
					data-w320="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
					data-w480="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
				alt="..."/>
			</div>
		</div>
				
		<div class="cols-row begin end">
				<img src="<?php echo _INSTDIR_; ?>img/banner-only-best.jpg" class="width-100 block"alt="">		
		</div>	
				
		<?php include_once("footer.php"); ?>
		
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>