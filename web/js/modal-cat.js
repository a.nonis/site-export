jQuery(document).ready(function($){
	//open popup
	$('.cd-popup-trigger').on('click', function(event){
        var t = $(this).data("category");
        console.log(t);
        
		$.ajax({
			url: 'inc/functions.ajax.php',
			data: {
				action: "getCat",
				id: $(this).data("ref"),
				category: $(this).data("category")
			},
			success: function (data) {
				obj = JSON.parse(data);
				if (obj.cat == "wet") {
					if (obj.product_1) {
						$("#modal_wet_composition").html("<h3>"+ obj.product_1 +"</h3>" + 
														   "<p>"+ obj.composition_1 +"</p>" +
														   "<p>"+ obj.additives_1 +"</p>" +
														   "<p>"+ obj.constituents_1 +"</p>" +
														   "<h3>"+ obj.product_2 +"</h3>" +
														   "<p>"+ obj.composition_2 +"</p>" +
														   "<p>"+ obj.additives_2 +"</p>" +
														   "<p>"+ obj.constituents_2 +"</p>");
					} else {
						$("#modal_wet_composition").html("<p>"+ obj.composition +"</p>" +
														   "<p>"+ obj.additives +"</p>" +
														   "<p>"+ obj.constituents +"</p>");
					}
					$('#modal_primary_img').attr('src', "img/cat/" + obj._id + ".png");
					$('#modal_category').addClass('label-vert');
					$('#modal_footer').addClass('footer-vert');
				} else {
					$('#modal_primary_img').attr('src', "img/cat/" + obj._id + ".png");		
					$('#modal_category').addClass('label-bleu');
					$('#modal_footer').addClass('footer-bleu');
				}
				$("#modal_category").html(obj.category);
				$("#modal_title").html(obj.product_name);
				$("#modal_compo").html(obj.composition);
				$("#modal_additives").html(obj.additives);
				$("#modal_constituents").html(obj.constituents);
				$('#modal_secondary_img').attr('src', 'img/formules/' + obj.formula + ".png");
				$("#modal_weight").html(obj.weight);
			}
		});

		event.preventDefault();
		$('.cd-popup').addClass('is-visible');
		$('.cd-popup-container').scrollTop(0);
	});

	$('.keradog-popup').on('click', function(event) {		
		event.preventDefault();
		$('.cd-popup').addClass('is-visible');
		$('.cd-popup-container').scrollTop(0);
	});
	
	//close popup
	$('.cd-popup').on('click', function(event){
		if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
		}
	});
    
	//close popup when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		$('.cd-popup').removeClass('is-visible');
	    }
    });
});