jQuery(document).ready(function($){

	// Open popup for producs
	$('.cd-popup-trigger').on('click', function(event){
		$.ajax({
			url: 'inc/functions.ajax.php',
			data: {
				action: "getDog",
				id: $(this).data("ref"),
				category: $(this).data("category")
			},
			success: function (data) {
				obj = JSON.parse(data);
				if (obj.category == "Grain Free") {
					$('#modal_primary_img').attr('src', "img/grain_free/" + obj._id + ".png");
					$('#modal_category').addClass('label-vert');
					$('#modal_footer').addClass('footer-vert');
				} else {
					$('#modal_primary_img').attr('src', "img/low_grain/" + obj._id + ".png");		
					$('#modal_category').addClass('label-bleu');
					$('#modal_footer').addClass('footer-bleu');
				}
				$("#modal_category").html(obj.category);
				$("#modal_title").html(obj.product_name);
				$("#modal_compo").html(obj.composition);
				$("#modal_additives").html(obj.additives);
				$("#modal_constituents").html(obj.constituents);
				$('#modal_secondary_img').attr('src', 'img/formules/' + obj.formula + ".png");
				$("#modal_weight").html(obj.weight);
			}
		});

		event.preventDefault();
		$('.cd-popup').addClass('is-visible');
		
		$('.cd-popup-container').scrollTop(0);
	});

	// Open popup for Keradog page
	$('.keradog-popup-1').on('click', function(event) {
		event.preventDefault();
		$('.cd-popup-1').addClass('is-visible');
		$('.cd-popup-container').scrollTop(0);
	});
	$('.keradog-popup-2').on('click', function(event) {
		event.preventDefault();
		$('.cd-popup-2').addClass('is-visible');
		$('.cd-popup-container').scrollTop(0);
	});
	$('.keradog-popup-3').on('click', function(event) {
		event.preventDefault();
		$('.cd-popup-3').addClass('is-visible');
		$('.cd-popup-container').scrollTop(0);
	});
	
	//close popup
	$('.cd-popup').on('click', function(event){
		if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
		}
	});

	// Close popup for keradog page
	$('.cd-popup-1').on('click', function(event){
		if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup-1') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
		}
	});
	$('.cd-popup-2').on('click', function(event){
		if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup-2') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
		}
	});
	$('.cd-popup-3').on('click', function(event){
		if( $(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup-3') ) {
			event.preventDefault();
			$(this).removeClass('is-visible');
		}
	});
    
});