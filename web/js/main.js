// Script de Jerome Debray pour la detection des devices : plugin jquery responsive image
// http://www.debray-jerome.fr/plugin-jquery-responsive-image-15.html


;(function(){$.fn.jResponsiveImageLoader=function(e){var a=e||{},h=null,b=$(this),c=null;if(typeof a.event=="undefined"){a.event="load"}if(typeof a.resize=="undefined"){a.resize=true}if(typeof a.file=="undefined"){a.detection="resolution"}else{a.detection="bandwidth"}if(typeof a.map=="undefined"){if(a.detection==="bandwidth"){a.map=[128,512,1024,4096,10240,20480]}else{a.map=[320,480,800]}}function i(n){var m=window.innerWidth,l=0,j=n.length,k="0";for(;l<j;l++){if(parseInt(m,10)>=parseInt(n[l])){k="w"+n[l]}}if(k==="0"){k="original"}return k}function d(m,k){var o=new Image(),p=0,n=0,q=k.length,s="0",r=0,j=0,l=0;o.src=m.src+"?_"+new Date().getTime();p=new Date().getTime();o.onload=function(){r=new Date().getTime(),j=(r-p)/1000,l=Math.ceil(m.weight/j);for(;n<q;n++){if(l>=k[n]){if(n===q){s="b"+k[n]}else{s="b"+k[n+1]}}}if(s==="bundefined"){s="original"}h=s;return s}}function g(o,n){var j=o.data(n),k=o,q=$(window).height(),m=o.offset(),l=false,p;o.css("opacity",0);if(typeof j==="object"||typeof j=="undefined"){j=o.data("original")}p=$(document).scrollTop()+q;if(m.top<=p){k.attr("src",j).animate({opacity:1},"slow");l=true}$(document).scroll(function(){if(l===false){p=$(this).scrollTop()+q;m=k.offset();if(m.top<=p){k.attr("src",j).animate({opacity:1},"slow");l=true}}})}function f(l){var k="0",j=null;if(l.detection==="bandwidth"){d(l.file,l.map);if(j===null){clearInterval(j)}j=setInterval(function(){if(h!==null){clearInterval(j);k=h;b.each(function(){g($(this),k)})}},10)}else{k=i(l.map);return b.each(function(){g($(this),k)})}}$(window).on(a.event,function(){f(a)});if(a.resize!==false){$(window).on("resize",function(){if(c!==null){clearTimeout(c)}c=setTimeout(function(){f(a)},100)})}}})();


// Mes scripts

jQuery(document).ready(function($){


    $('.lazy').jResponsiveImageLoader({
        event : 'load',
        detection : 'resolution',
        map : [320, 480, 800, 1024],
        resize : true
    });


  if( $('.floating-labels').length > 0 ) floatLabels();

  function floatLabels() {
    var inputFields = $('.floating-labels .cd-label').next();
    inputFields.each(function(){
      var singleInput = $(this);
      //check if user is filling one of the form fields 
      checkVal(singleInput);
      singleInput.on('change keyup', function(){
        checkVal(singleInput);  
      });
    });
  }

  function checkVal(inputField) {
    ( inputField.val() == '' ) ? inputField.prev('.cd-label').removeClass('float') : inputField.prev('.cd-label').addClass('float');
  }
});

/* 
 * CONTACT
 */ 

$("form#contact").submit(function() {

  var contact_name = $("form#contact input[name='contact_name']").val();
  var contact_mail = $("form#contact input[name='contact_mail']").val();
  var contact_message = $("form#contact textarea[name='contact_message']").val();


  $.ajax({
    type: "POST",
    url: "inc/functions.ajax.php",
    data: {
      "action":"contact",
      "contact_name":contact_name,
      "contact_mail":contact_mail,
      "contact_message":contact_message
    },
    success: function(msg){
      if (msg == "OK") {
        $("#report-erreur").html('<div class="success-message"><p>Your message has been successfully sent</p> </div>');
      } else {
        $("#report-erreur").html('<div class="error-message"><p>'+msg+'</p> </div>');
      }
    }
  });
  return false;
});

/* 
 * RESELLER
 */ 

$("form#reseller").submit(function() {

  $("#loader").show();

  var firstname = $("form#reseller input[name='firstname']").val();
  var lastname = $("form#reseller input[name='lastname']").val();
  var company = $("form#reseller input[name='company']").val();
  var tax = $("form#reseller input[name='tax']").val();
  var phone = $("form#reseller input[name='phone']").val();
  var email = $("form#reseller input[name='email']").val();
  var address = $("form#reseller input[name='address']").val();
  var city = $("form#reseller input[name='city']").val();
  var state = $("form#reseller input[name='state']").val();
  var postalcode = $("form#reseller input[name='postalcode']").val();
  var contry = $("form#reseller input[name='contry']").val();


  if ($("#checkcustomer").is(':checked')) {
    var customer = $("form#reseller input[name='customer']").val();
  }

  if ($("#checkwholesale").is(':checked')) {
    var wholesale = $("form#reseller input[name='wholesale']").val();
  }

  if ($("#checkinternet").is(':checked')) {
    var internet = $("form#reseller input[name='internet']").val();
  }

  if ($("#checkshop").is(':checked')) {
    var shop = $("form#reseller input[name='shop']").val();
  }

  var commentaire = $("form#reseller textarea[name='commentaire']").val();


  $.ajax({
    type: "POST",
    url: "inc/functions.ajax.php",
    data: {
      "action":"reseller",
      "firstname" : firstname,
      "lastname" : lastname,
      "company" : company,
      "tax" : tax,
      "phone" : phone,
      "email" : email,
      "address" : address,
      "city" : city,
      "state" : state,
      "postalcode" : postalcode,
      "contry" : contry,
      "customer" : customer,
      "wholesale" : wholesale,
      "internet" : internet,
      "shop" : shop,
      "commentaire" : commentaire    
    },
    success: function(msg){
      if (msg == "OK") {
        $("#report-erreur").html('<div class="success-message"><p>Your message has been successfully sent</p> </div>');
        $("#loader").hide();
        $("form#reseller input[type='submit']").hide();
      } else {
        $("#report-erreur").html('<div class="error-message"><p>'+msg+'</p> </div>');
        $("#loader").hide();
      }
    }
  });
  return false;
});