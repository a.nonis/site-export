<?php 
require_once ("inc/config.php");
require_once ("inc/functions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="en" class="no-js">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">
		<title>Natura Plus Ultra</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		
		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://en.naturaplusultra.com"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content=""/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>

		<?php include_once("header.php"); ?>

		<div class="container grain-free">

			<div class="cols-row">
				<div class="col-60">
					<h1>
						404 - <strong>Page Not Found</strong>
					</h1>
				</div>
				<div class="col-40 no-mob">
					
				</div>
			</div>
			
		</div>
		
		<div class="product-gamme cols-row end begin ekomi">

			<div class="container begin end">
				<img class="width-100 block lazy" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" 
				    data-original="<?php echo _INSTDIR_; ?>img/ekomi.jpg" 
				    data-w320="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
				    data-w480="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
				alt="..."/>
			</div>

		</div>

		<div class="cols-row begin end">
				<img src="<?php echo _INSTDIR_; ?>img/banner-only-best.jpg" class="width-100 block"alt="">		
		</div>	
				
		<?php include_once("footer.php"); ?>
		
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>