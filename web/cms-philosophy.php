<?php 
require_once ("inc/config.php");
require_once ("inc/functions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="en" class="no-js">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">
		<title>Ultra Premium direct</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		
		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://en.naturaplusultra.com"/>
		<meta property="og:title" content=""/> 
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content=""/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>

		<?php include_once("header.php"); ?>
		
		<!-- 1er block -->
		<div class="cols-row begin end">
			<div class="philo-header">
				<img src="<?php echo _INSTDIR_; ?>img/philosophy/our_philosophy_croquettes.jpg" class="width-100"alt="">
				<div class="philo-header-text">
					<div class="cols-row philo-mob">
						<div class="col-100">
							<div class="philo-heading-primary">
								<h1>100% HIGH QUALITY INGREDIENTS</h1>
							</div>
						</div>
					</div>
					<div class="cols-row philo-desktop">
						<div class="col-50">
							<div class="philo-heading-primary">
								<h1>100% HIGH QUALITY INGREDIENTS</h1>
							</div>
						</div>
						<div class="col-50">
							<div class="philo-heading-sub">
								<ul>
									<li>MEAT & FISH INGREDIENTS IN ALL OUR RECIPES</li>
									<li>100% HIGH QUALITY INGREDIENTS</li>
									We are convinced that diet has a huge impact on pets health and well-being. That is why we propose the best quality for complete and balanced premium food. Our companion dogs and cats are carnivore. All our formulas are based on pet’s ancestral diet. That is why our recipes are rich in meat and protein of animal origin just as nature intended. Our recipes are veterinary formulated. Our first ingredient is and will remain meat. Our formulas have a low glycemic index to prevent risks from diabetes and obesity: low grain (mainly rice) or grain free.
									<li>PREMIUM PET FOOD : GLUTEN FREE</li>
									No artificial colors, flavors or preservatives. Neither wheat nor soy. No by-products.
									<li>VETERINARY FORMULATED</li>
									Each formula was carefully developed by a team of nutritionists veterinarians, who select the best ingredients that offer optimum benefits specific to your pet.
								</ul>
							</div>		
						</div>
					</div>

				</div>
			</div>
		</div>	


		<div class="container">
			<div class="cols-row">
				<div class="col-100">
					<div class="philo-heading-mob">
						<ul>
							<li>MEAT & FISH INGREDIENTS IN ALL OUR RECIPES</li>
							<li>100% HIGH QUALITY INGREDIENTS</li>
							We are convinced that diet has a huge impact on pets health and well-being. That is why we propose the best quality for complete and balanced premium food. Our companion dogs and cats are carnivore. All our formulas are based on pet’s ancestral diet. That is why our recipes are rich in meat and protein of animal origin just as nature intended. Our recipes are veterinary formulated. Our first ingredient is and will remain meat. Our formulas have a low glycemic index to prevent risks from diabetes and obesity: low grain (mainly rice) or grain free.
							<li>PREMIUM PET FOOD : GLUTEN FREE</li>
							No artificial colors, flavors or preservatives. Neither wheat nor soy. No by-products.
							<li>VETERINARY FORMULATED</li>
							Each formula was carefully developed by a team of nutritionists veterinarians, who select the best ingredients that offer optimum benefits specific to your pet.
						</ul>
					</div>
				</div>
			</div>
		</div>


		<!-- 2eme block -->
		<div class="container">
			<div class="cols-row end">
				<div class="col-100 centered philosophy--ekomi">
					<h1 class="philosophy--factory-text">Strong pet food expertise</h1>
				</div>
			</div>
			<div class="cols-row">
				<div class="col-50">
					<div class="cols-row begin philo-container">
						<div class="col-70 philo-bloc">
							<ul>
								<li>
									Factory of 10 000 m2 and head office located in the South West of France : 
								</li>
								- Approved site by veterinary department.
								<li>
									Dog and Cat premium food manufacturing and marketing.
								</li>
								- Veterinary formulated.<br>
								- Gluten Free.
							</ul>
						</div>
						<div class="col-30 no-mob">
							<img src="<?php echo _INSTDIR_; ?>img/philosophy/carte_france.png" class="map-france" alt="">
						</div>
					</div>
				</div>
				<div class="col-50 begin">
					<img src="<?php echo _INSTDIR_; ?>img/homepage/our_philosophy_homepage_2.jpg" alt="" class="show"> 
				</div>
			</div>
		</div>
			
		<!-- 3eme block -->
		<div class="width-100 grey-back">
			<div class="container">
				<div class="products">
					<div class="cols-row">
						<div class="col-90 col-centre">
							<div class="col-33 centered">
								<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food">
									<div class="products__item">
										<img src="<?php echo _INSTDIR_; ?>img/low_grain/gamme_low_grain.png" alt="">
									</div>
								</a>
								<h4 class="products__name">Low grain dog food</h4>
								<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food" class="btn upper width-100">Learn more ></a>
							</div>
			
							<div class="col-33 centered">
								<a href="<?php echo _INSTDIR_; ?>grain-free-dry-dog-food">
									<div class="products__item">
										<img src="<?php echo _INSTDIR_; ?>img/grain_free/gamme_grain_free.png" alt="">
									</div>
								</a>
								<h4 class="products__name">Grain free dog food</h4>
								<a href="<?php echo _INSTDIR_; ?>grain-free-dry-dog-food" class="btn upper width-100">Learn more ></a>
							</div>
			
							<div class="col-33 centered">
								<a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food">
									<div class="products__item">
										<img src="<?php echo _INSTDIR_; ?>img/cat/gamme_dry_wet_chat.png" alt="">
									</div>
								</a>
								<h4 class="products__name">Grain free cat food</h4>
								<a href="<?php echo _INSTDIR_; ?>grain-free-dry-cat-food" class="btn upper width-100">Learn more ></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="product-gamme cols-row end begin ekomi">

			<div class="container begin end">
				<img class="ekomi-img block lazy" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" 
				    data-original="<?php echo _INSTDIR_; ?>img/ekomi.jpg" 
				    data-w320="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
				    data-w480="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
				alt="..."/>
			</div>

		</div>

		<div class="cols-row begin end no-mob">
				<img src="<?php echo _INSTDIR_; ?>img/banner-only-best.jpg" class="width-100 block"alt="">		
		</div>	
				
		<?php include_once("footer.php"); ?>
		
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>