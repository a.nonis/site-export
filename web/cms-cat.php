<?php 
require_once ("inc/config.php");
require_once ("inc/functions.php");
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<html lang="en" class="no-js">

	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1,minimum-scale=1,width=device-width">
		<title>Ultra Premium direct</title>

		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		
		<!-- Open Graph facebook -->
		<meta property="og:url" content="http://en.naturaplusultra.com"/>
		<meta property="og:title" content=""/>
		<meta property="og:type" content="website"/> 
		<meta property="og:image" content=""/> 
		<meta property="og:site_name" content=""/> 
		<meta property="og:description" content=""/>

		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/modal-cat.js"></script>
		<link rel="stylesheet" href="<?php echo _INSTDIR_; ?>css/modal.css">

		<?php include_once("header.php"); ?>

		<!-- 1er block - Image -->
		<div class="width-100">
			<img src="<?php echo _INSTDIR_; ?>img/cat/grain_free_cat_dry_page.jpg" class="width-100 block"alt="">		
		</div>

		<!-- Presentation texte -->
		<div class="container">
			<div class="presentation-text">
				<p>
				Our companion cats are carnivore. All of our formulas are based on pet’s ancestral diet. That is why our recipes are rich in meat and protein of animal origin just as nature intended. 
				<br><br>
				Our recipes are veterinary formulated. Our first ingredient is and will remain meat. Our formulas have a low glycemic index to prevent risks from diabetes and obesity: grain free. Ingredients are guaranteed gluten free. 
				</p>
			</div>
		</div>

		<!-- 2eme block - Products -->
		<div class="container">
			<div class="products">
				<?php 
					$json = file_get_contents("products/cat/cat-dry.json");
					$data = json_decode($json);
					$i = 0;
				?>
				<ul class="grid-products">
				<?php foreach($data as $key => $val): ?>
					<li>
						<div class="products__item">
							<img src="<?php echo _INSTDIR_; ?>img/cat/<?= $val->_id ?>.png" alt="" class="cd-popup-trigger" data-ref="<?= $key ?>" data-category="dry">
						</div>
						<h4 class="products__name"><?= $val->product_name ?></h4>
						<a href="<?php echo _INSTDIR_; ?>low-grain-dry-dog-food" class="btn upper width-100 products__label--violet cd-popup-trigger" data-ref="<?= $key ?>" data-category="dry">Available: <?= $val->weight ?> <span id="arrow_anim">></span></a>
					</li>
				<?php endforeach ?>
					<li>
						<a href="<?php echo _INSTDIR_; ?>made-without-grain-wet-cat-food">
							<div class="products__item">
								<img src="<?php echo _INSTDIR_; ?>img/cat/gamme_wet_chat.png" alt="">
							</div>
						</a>
						<h4 class="products__name">MADE WITHOUT GRAIN WET CAT FOOD</h4>
						<a href="<?php echo _INSTDIR_; ?>made-without-grain-wet-cat-food" class="btn products__label--violet upper width-100">Learn more ></a>
					</li>
				</ul>
			</div>
		</div>

		<!-- 3eme block - Image skills-->
		<div class="width-100">
			<div class="products-skills-violet no-mob">
				<div class="container begin">
					<img src="<?php echo _INSTDIR_; ?>img/cat/wet_cat_gamme_details.jpg" class="width-100 block"alt="">
				</div>
			</div>
		</div>

		<!-- 4eme block - Avis -->
		<div class="product-gamme cols-row end begin ekomi">
			<div class="container begin end">
				<img class="ekomi-img block lazy" src="data:image/gif;base64,R0lGODlhAQABAID/AMDAwAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" 
				    data-original="<?php echo _INSTDIR_; ?>img/ekomi.jpg" 
				    data-w320="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
				    data-w480="<?php echo _INSTDIR_; ?>img/ekomi_480.jpg" 
				alt="..."/>
			</div>
		</div>

		<!-- Modal -->
		<div class="cd-popup">
			<div class="cd-popup-container">
				<div class="container modal-container">
					<!-- 1er block -->
					<div class="cols-row">
						<div class="col-40 center-img">
							<img id="modal_primary_img" src="" alt="">
						</div>
						<div class="col-60 margin-top-medium">
							<div class="cols-row">
								<div class="col-100">
									<label id="modal_category" class="modal-label label-violet"></label>
									<span class="weight-label margin-left-small bold color-violet">Available: <span id="modal_weight"></span></span>
								</div>
							</div>
							<div class="cols-row">
								<div class="col-100">
									<h1 id="modal_title" class="modal-title"></h1>
								</div>
							</div>
							<div class="cols-row">
								<div class="col-100">
									<p id="modal_compo"></p>
								</div>
							</div>
						</div>
					</div>

					<!-- 2eme block -->
					<div class="cols-row">
						<div class="col-100">
							<strong>NUTRITIONAL ADDITIVES (by Kg) :</strong><p id="modal_additives"></p>
						</div>
					</div>

					<!-- 3eme block -->
					<div class="cols-row">
						<div class="col-80">
							<p id="modal_constituents"></p>
						</div>
						<div class="col-20 center-img">
							<img id="modal_secondary_img" src="" alt="">
						</div>
					</div>
				</div>
				<a href="#0" class="cd-popup-close img-replace">Close</a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->

		<!-- 5eme block - Image -->
		<div class="cols-row begin end no-mob">
				<img src="<?php echo _INSTDIR_; ?>img/banner-only-best.jpg" class="width-100 block"alt="">		
		</div>


				
		<?php include_once("footer.php"); ?>
		
		<script src="<?php echo _INSTDIR_; ?>js/jquery.min.js"></script>
		<script src="<?php echo _INSTDIR_; ?>js/main.js"></script>

	</body>
</html>