# Routage hors France

Dans Prestashop 1.5 de Natura, il faut aller dans classes>controller>FrontController.php à la ligne 598 et mettre

`
protected function displayRestrictedCountryPage()
{
	// header('HTTP/1.1 503 temporarily overloaded');
	header('http://en.naturaplusultra.com');
	$this->context->smarty->assign('favicon_url', _PS_IMG_.Configuration::get('PS_FAVICON'));
	$this->smartyOutputContent(_PS_THEME_DIR_.'restricted-country.tpl');
	exit;
}
`