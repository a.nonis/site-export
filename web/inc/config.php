<?php
/**
 * DEBUG : Enable
 */

error_reporting(E_ALL ^ E_NOTICE);
ini_set("display_errors", 1); 

/**
 * TimeZone
 */

// List of supported Timezones: http://www.php.net/manual/en/timezones.php
date_default_timezone_set('Europe/Vienna');
setlocale(LC_TIME, "fr_FR");


$currentDir = dirname(__FILE__);
define('_ROOT_DIR_', realpath($currentDir.'/..'));
define('_INC_DIR_', _ROOT_DIR_.'/inc/');

require_once _ROOT_DIR_.'/vendor/autoload.php';

/**
 * MailJet
 */

// API Key of MailJet - Compte Ultra Premium Direct
define('_MAILJET_API_KEY_', 'fef6ae8b4a8043cf3e677403dcc6b8bf'); 
define('_MAILJET_API_SECRET_', '3b06d9b718164d076cc6bc368dd3086e');

define('_ADMIN_EMAIL_', 'contact@ultrapremiumdirect.com');
define('_ADMIN_NAME_', 'Contact | Ultrapremiumdirect.com');

/**
 * Directory
 */

$instDir = dirname($_SERVER["SCRIPT_NAME"]);
if ($instDir == "/") unset($instDir);
define('_INSTDIR_', $instDir."/");
define('_URLDIR_', $_SERVER['HTTP_HOST']);
define('_URL_', $_SERVER['REQUEST_URI']);
define('_DOMAIN_', 'http://localhost:8888/naturaplusultra/');

$f = basename($_SERVER['PHP_SELF']);