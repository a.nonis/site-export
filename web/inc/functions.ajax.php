<?php
include_once("config.php");
require_once ("functions.php");

$action = $_REQUEST["action"];

if ($action == "contact") {

	$contact_name = trim(stripslashes($_POST['contact_name']));
	$contact_mail = trim(strtolower($_POST['contact_mail']));
	$contact_message = trim(stripslashes($_POST['contact_message']));

	$error = array();
	if(!$contact_mail) $error[] = "Mail is empty";
	if(!$contact_name) $error[] = "Name is empty";
	if(!$contact_message) $error[] = "Message is empty";

	if(!$error) {

	$HtmlPart = '
	<h3>Message</h3>

	<p>'.$contact_message.'</p>

	<h3>Informations Expéditeur</h3>

	<p>'.$contact_name.'<br>
	'.$contact_mail.' </p>
	';

	appSendMail("Natura PLus Ultra - Message", $HtmlPart, "pro@ultrapremiumdirect.com", "Sophie Wincker");
	
	if(!$in_site) exit("OK");

	} else {
		$error = implode(" <br> ",$error);
		if (!$in_site) echo $error;
	}
}

if ($action == "reseller") {

	$reseller_firstname = trim(stripcslashes($_POST['firstname']));
	$reseller_lastname = trim(stripcslashes($_POST['lastname']));
	$reseller_company = trim(stripcslashes($_POST['company']));
	$reseller_tax = trim(stripcslashes($_POST['tax']));
	$reseller_phone = trim(stripcslashes($_POST['phone']));
	$reseller_email = trim(stripcslashes($_POST['email']));
	$reseller_address = trim(stripcslashes($_POST['address']));
	$reseller_city = trim(stripcslashes($_POST['city']));
	$reseller_state = trim(stripcslashes($_POST['state']));
	$reseller_postalcode = trim(stripcslashes($_POST['postalcode']));
	$reseller_contry = trim(stripcslashes($_POST['contry']));
	$reseller_customer = trim(stripcslashes($_POST['customer']));
	$reseller_wholesale = trim(stripcslashes($_POST['wholesale']));
	$reseller_internet = trim(stripcslashes($_POST['internet']));
	$reseller_shop = trim(stripcslashes($_POST['shop']));
	$reseller_commentaire = trim(stripcslashes($_POST['commentaire']));

	$error = array();

	if(!$reseller_email) $error[] = "email is empty";

	if(!$error) {

	$HtmlPart = '
	<h3>Reseller Form</h3>


	<h4>Personnal Informations</h4>
	<p>
	<strong>Firstname :</strong> '.$reseller_firstname.' <br>
	<strong>Lastname :</strong> '.$reseller_lastname.' <br>
	<strong>Company :</strong> '.$reseller_company.' <br>
	<strong>Tax :</strong> '.$reseller_tax.' <br>
	<strong>Phone :</strong> '.$reseller_phone.' <br>
	<strong>Email :</strong> '.$reseller_email.'
	</p>
	
	<h4>Address</h4>
	<p>
	<strong>Address :</strong> '.$reseller_address.' <br>
	<strong>City :</strong> '.$reseller_city.' <br>
	<strong>State :</strong> '.$reseller_state.' <br>
	<strong>Postalcode :</strong> '.$reseller_postalcode.' <br>
	<strong>Country :</strong> '.$reseller_contry.'
	</p>

	<h4>About your business</h4>
	<p>
	<strong>Customer :</strong> '.$reseller_customer.' <br>
	<strong>Wholesale :</strong> '.$reseller_wholesale.' <br>
	<strong>Internet :</strong> '.$reseller_internet.' <br>
	<strong>Shop :</strong> '.$reseller_shop.' <br>
	<strong>Commentaire :</strong> '.nl2br($reseller_commentaire).'
	</p>
	';

	appSendMail("Natura PLus Ultra - Message", $HtmlPart, "pro@ultrapremiumdirect.com", "Sophie Wincker");
	
	if(!$in_site) exit("OK");

	} else {
		$error = implode(" <br> ",$error);
		if (!$in_site) echo $error;
	}
}

if ($action == "getDog") {

	$category = $_REQUEST['category'];
	$id = $_REQUEST['id'];

	if (!isset($category) || !isset($id)) {
		exit("error");
	}

	if ($category == "grain_free") {
		$file = '../grain_free/grain_free_products.json';
	} elseif ($category == "low_grain") {
		$file = '../low_grain/low_grain_products.json';
	} else {
		exit('error');
	}

	$json = file_get_contents($file);
	$data = json_decode($json);

	$result = $data[$id];
    
	echo json_encode($result);
}

if ($action == "getCat") {

	$category = $_REQUEST['category'];
	$id = $_REQUEST['id'];

	if (!isset($category) || !isset($id)) {
		exit("error");
	}

	if ($category == "wet") {
		$file = '../products/cat/cat-wet.json';
	} elseif ($category == "dry") {
		$file = '../products/cat/cat-dry.json';
	} else {
		exit('error');
	}

	$json = file_get_contents($file);
	$data = json_decode($json);

	$result = $data[$id];
    
	echo json_encode($result);
}