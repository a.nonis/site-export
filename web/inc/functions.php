<?php 

use \Mailjet\Resources;

/**
 * Send e-mail with Mandrillapp.
 * Mandrillapp settings in : config/settings.inc.php
 * 
 * @link(API Documentation about sending message, https://mandrillapp.com/api/docs/messages.php.html)
 * @link(Twitter Emoji (Twemoji) Preview, http://twitter.github.io/twemoji/preview.html)
 * 
 * @param  string $nameFrom Sender name
 * @param  string $mailForm Sender e-mail
 * @param  string $subject  E-mail Subject
 * @param  string $message  E-mail Message
 * @param  string $mailTo   Receiver e-mail
 * @param  string $nameTo   Receiver name
 * 
 * @return                  Send the e-mail
 **/ 

function appSendMail ($subject, $HtmlPart, $ToEmail, $ToName) {

    // Sanitize variables
    $subject = trim(stripslashes($subject));
    $HtmlPart = trim(stripslashes($HtmlPart));
    $ToEmail = trim(strtolower($ToEmail));
    $ToName = trim(stripslashes($ToName));

    $mailjet = new \Mailjet\Client(_MAILJET_API_KEY_, _MAILJET_API_SECRET_);
    $body = [
        'FromEmail' => _ADMIN_EMAIL_,
        'FromName' => _ADMIN_NAME_,
        'Subject' => $subject,
        'Html-part' => $HtmlPart,
        'Recipients' => [
            [
                'Email' => $ToEmail,
                'Name' => $ToName
            ]
        ]
    ];
    $response = $mailjet->post(Resources::$Email, ['body' => $body]);
}

/**
 * Redirect to link page
 * @param string $url - URl to redirect
 * @return go to the URL
 **/ 

function redirect($url){

    header("Location: $url");
    exit;

}